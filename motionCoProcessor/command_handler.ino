#if DEBUG 
#define TRACE(x) Serial.println(x);  
                 
#else
#define TRACE(x) {};
#endif

#define X_BOUND 20000 //40 - 10 - 10 (5cm per side buffer)
#define Y_BOUND 33500 //53.5 - 10 - 10cm (5cm per side buffer)
#define Z_BOUND 60    //14 - 6 - 2cm

enum class axis {x,y,z};
enum class command {MOV,STP,RST,ENG,DNG};
const float STEPS_PER_MM = 318.5; //318.5 steps/mm

byte commandHandler(String cmd, float dat[], int datSize);
char handleMOV(float dat[], int datSize);
void moveMotor(axis dir, float dist);
int boundsCheck(float x, float y, float z);
char handleSTP();
char handleRST();
char handleENG();
char handleDNG();

byte commandHandler(String cmd, float dat[], int datSize){
  int eventCode = 3;
 
  if (cmd == "MOV"){
    eventCode = handleMOV(dat, datSize);
  }
  else if (cmd == "STP"){
    eventCode = handleSTP();
  }    
  else if (cmd == "RST"){
    eventCode = handleRST();
  }
  else if (cmd == "ENG"){
    eventCode = handleENG();
  } 
  else if (cmd == "DNG"){
    eventCode = handleDNG();
  } 
  else {
    eventCode = 3;
  }   
  return eventCode;
}

char handleMOV(float dat[], int datSize){  
  const int eventResult = 100;
  float x_dist = dat[0];
  float y_dist = dat[1];
  float z_dist = dat[2];
  
  if(boundsCheck(x_dist,y_dist,z_dist) == true){
    if(x_dist != 0){
      moveMotor(axis::x,x_dist);
    }
    if(y_dist != 0){
      moveMotor(axis::y,y_dist);
    }
    if(z_dist != 0){
      moveMotor(axis::z,z_dist);
    }
  }
  else{
    eventResult = (unsigned int)-1;  
  }
  return eventResult;
}

boolean boundsCheck(float x, float y, float z){

  boolean retStat = true;
  
  float cur_x_pos = EEPROMReadlong(X_ADDR) / STEPS_PER_MM;
  delay(10);
  float cur_y_pos = EEPROMReadlong(Y_ADDR) / STEPS_PER_MM;
  delay(10);
  float cur_z_pos = EEPROMReadlong(Z_ADDR) / STEPS_PER_MM;
  delay(10);

  float proposedPosX = cur_x_pos + x;
  float proposedPosY = cur_y_pos + y;
  float proposedPosZ = cur_z_pos + z;  

  if(proposedPosX < 0 || proposedPosX > X_BOUND){
    retStat = false;
    TRACE("input value is over the X bound!");
  }
  if(proposedPosY < 0 || proposedPosY > Y_BOUND){
    TRACE("input value is over the Y bound!");
    retStat = false;
  }
  if(proposedPosZ < 0 || proposedPosZ > Z_BOUND){
    TRACE("input value is over the Z bound!");
    retStat = false;
  }
  return retStat;  
}

void moveMotor(axis dir, float dist, boolean record){
  int STP_PIN, DIR_PIN, ENA_PIN, ADDR;  
  if(dir == axis::x){
     STP_PIN = X_STP;
     DIR_PIN = X_DIR;
     ENA_PIN = X_ENA;
     ADDR = X_ADDR;
     TRACE("X dir specified");
  }
  else if (dir == axis::y){
    STP_PIN = Y_STP;
    DIR_PIN = Y_DIR;
    ENA_PIN = Y_ENA;
    ADDR = Y_ADDR;
    TRACE("Y dir specified");
  }
  else if (dir == axis::z){
    STP_PIN = Z_STP;
    DIR_PIN = Z_DIR;
    ENA_PIN = Z_ENA;
    ADDR = Z_ADDR;
    TRACE("Z dir specified");
  }
  else{
    TRACE("moveMotors error: invalid direction specified");
  }

  //determine magnitude and direction
  int dirVal = LOW;
  long stepCount = 0;
  long curpos = 0;
  long prevpos = 0;
  long dirVec = 0;
  if(dist < 0){
    if(dir == axis::y){
      dirVal = LOW;
    }
    else{
      dirVal = HIGH;
    }
    stepCount = (long)(dist * STEPS_PER_MM * -1.0);
    dirVec = -1.0;
  }
  else if (dist > 0){
    if(dir == axis::y){
      dirVal = HIGH;
      
    }
    else{
      dirVal = LOW;
    }
    stepCount = (long)(dist * STEPS_PER_MM);
    dirVec = 1.0;
  }
  else{
    TRACE("moveMotors error: 0 distance specified");
    return;
  }
  
  Serial.println(" ");
  Serial.print(dirVal);
  Serial.print(" ");
  Serial.print(stepCount);
  Serial.print(" ");
  Serial.println(dist);
  int i = 0;
  prevpos = EEPROMReadlong(ADDR);
  for (i=0; i<stepCount; i++)    
  {
    digitalWrite(DIR_PIN,dirVal);
    digitalWrite(ENA_PIN,LOW);
    digitalWrite(STP_PIN,HIGH);
    delayMicroseconds(120);
    digitalWrite(STP_PIN,LOW);
    delayMicroseconds(120);
    if(i%1000 == 0){
      curpos = prevpos + (i * dirVec) ;
      EEPROMWritelong(ADDR,curpos);
    }
  }
  EEPROMWritelong(ADDR,curpos); 
}

char handleSTP(){
  int eventResult = 110;
  return eventResult;
}

char handleRST(){
  int eventResult = 120;
  return eventResult;
}

char handleENG(){
  int eventResult = 130;
  return eventResult;
}

char handleDNG(){
  int eventResult = 140;
  return eventResult;
}

void returnToZero(){
  float curXpos, curYpos, curZpos;
  curXpos = EEPROMReadlong(X_ADDR);
  curYpos = EEPROMReadlong(Y_ADDR);
  curZpos = EEPROMReadlong(Z_ADDR);

  if(curXpos != 0){
    moveMotor(axis::x, curXpos * -1.0);
    EEPROMWritelong(X_ADDR,0);
  }
  if(curYpos != 0){
    moveMotor(axis::y, curYpos * -1.0);
    EEPROMWritelong(Y_ADDR,0);
  }
  if(curZpos != 0){
    moveMotor(axis::z, curZpos * -1.0);
    EEPROMWritelong(Z_ADDR,0);
  } 
}


