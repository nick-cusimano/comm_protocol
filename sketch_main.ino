#define TIMEOUT 100 //time out value for serial reads in ms
#define NUM_CMDS 5  //used for the commandList -- allows for new commands to be added in the future (comm protocol may need expansion)
#define DEBUG 1     //enables trace statments
#define ZERO_CALIBRATE 0 //for manually calibrating the 0 position
#define RETURN_TO_ZERO 1 //for automatically zeroing upon every reset
#include <EEPROM.h>

const int X_STP=11; 
const int X_DIR=7;
const int X_ENA=4; 

const int Y_STP=10; 
const int Y_DIR=6; 
const int Y_ENA=3; 

const int Z_STP=9; 
const int Z_DIR=5; 
const int Z_ENA=2; 

const int X_ADDR = 0;
const int Y_ADDR = 4;
const int Z_ADDR = 8;


class commandFormat{
  public:
    String command;             //3 character name of the command (e.g. MOV, STP, RST, ENG, DNG) 
    byte numArgs;               //currently support 255 data arguments
    boolean allowedDataFormat;  //specifies if the data arguments should be populated with bools (true) or empty (false)
};

commandFormat commandList[NUM_CMDS];

void setUpCommandList();
void fillCommandStruct(int index, String commandName, byte numDataArgs, boolean fillData);
void EEPROMWritelong(int address, long value);
long EEPROMReadlong(long address);
long stepCount = -1234567;

void setup() {
  Serial.begin(9600);
  //Serial.setTimeout(TIMEOUT);
  setUpCommandList();  
  pinMode (X_DIR, OUTPUT);
  pinMode (X_STP, OUTPUT);
  pinMode (X_ENA, OUTPUT);
  
  pinMode (Y_DIR, OUTPUT);
  pinMode (Y_STP, OUTPUT);
  pinMode (Y_ENA, OUTPUT);
  
  pinMode (Z_DIR, OUTPUT);
  pinMode (Z_STP, OUTPUT);
  pinMode (Z_ENA, OUTPUT);

  if(ZERO_CALIBRATE){
    Serial.println("Calibrating EEPROM for this position to be the origin");
    Serial.println("WARNING: CHANGE THE X, Y, AND Z BOUNDS IF THIS IS A DIFFERENT ORIGIN THAN BEFORE!");
    EEPROMWritelong(X_ADDR,0);
    EEPROMWritelong(Y_ADDR,0);
    EEPROMWritelong(Z_ADDR,0);
  }
  if(RETURN_TO_ZERO){
    returnToZero();
  }
}

void loop() {
  parseEvent();
}

void setUpCommandList(){
  //to modify the command data field length, change the function calls to fill command struct
  //to add new commands, simply add a new call to fillCommandStruct() with the appropriate params (see comment section below)
  //IMPORTANT: to add new commands, you will also need to increment the NUM_CMD define at the top of this file. 
  
  fillCommandStruct(0,"MOV",3,true);
  fillCommandStruct(1,"STP",0,false);
  fillCommandStruct(2,"RST",0,false);
  fillCommandStruct(3,"ENG",0,false);
  fillCommandStruct(4,"DNG",0,false);

  //user to add commands here (format provided): 
  /*fillCommandStruct(5,[STING NAME OF YOUR COMMAND],[NUM ARGS OF YOUR COMMAND],[ARE YOUR ARG FIELDS POPULATED WITH FLOATS]); */ 
}

void fillCommandStruct(int index, String commandName, byte numDataArgs, boolean fillData){
  commandList[index].command = commandName;  
  commandList[index].numArgs = numDataArgs;
  commandList[index].allowedDataFormat = fillData;
}


